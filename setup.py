"""Sumall."""
from setuptools import setup, find_packages

setup(
    name='sumall',
    version='0.1',
    description='Sums all seqlen fields from matched files in a directory.',
    author='James Phillips',
    license='MIT',
    packages=find_packages(),
    author_email='jn_phillips@hotmail.com',
    requires_extra=[],
    entry_points={
        'console_scripts': [
            'sumall = sumall.sumall:main'
        ]
    }
)
