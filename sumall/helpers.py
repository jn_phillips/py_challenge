"""Helper functions."""
import os
import sys
import glob
import json
from json import JSONDecodeError


def list_files_from_pattern(directory, pattern='*.data.json'):
    """
    Get a list of file paths from a given directory.

    :param: directory.
    :param: patteren: optional glob pattern.
    """
    if not os.path.isdir(directory):
        sys.exit(f'{directory} isn\'t a valid directory.')
    return glob.glob(f"{directory}/{pattern}")


def map_file(func, **kwargs):
    """
    Open a file and passes the file object to a functions.

    :param: func: function to parse the file.
    :param: kwargs:
        :path: path to the file.
        :*: any other parameters you want to pass to the function.
    :return: 0 if file can't be opened or whatever the func returns.
    """
    try:
        with open(kwargs["path"], 'r') as content:
            kwargs["content"] = content
            return func(**kwargs)
    except IOError as e:
        print(f'Skipping file {kwargs["path"]}, {e}')
        return 0


def sum_field(**kwargs):
    """
    Given a file object and field name, sums all fields.

    :param: kwargs:
        :path: path to the file.
        :content: file object to parse.
        :field: field to sum.
    :return: sum of field from all lines.

    """
    sum = 0
    for idx, line in enumerate(kwargs["content"]):
        try:
            data = json.loads(line)
            sum += int(data[kwargs["field"]])
        except JSONDecodeError as e:
            print(f'Skipping line {idx + 1} in {kwargs["path"]}, {e}')
            continue
    return sum


def parse_arg(docstring):
    """
    Parse program input for directory path.

    :param: directory: optional, only used for testing.
    :param: docstring: usage text to return when no directory is given.
    :return: directory path.
    """
    if len(sys.argv) == 2:
        return str(sys.argv[1])
    else:
        sys.exit(docstring)
