"""Helper tests."""
import os

from io import StringIO
from unittest.mock import patch

import pytest

from sumall.helpers import parse_arg
from sumall.helpers import list_files_from_pattern
from sumall.helpers import map_file
from sumall.helpers import sum_field


def test_parse_args():
    with patch('sys.argv', ['sumall', 'test']):
        assert parse_arg("docs") == 'test'
    with patch('sys.argv', ['sumall']):
        with pytest.raises(SystemExit) as cap_err:
            parse_arg(docstring="docs")
        assert cap_err.type == SystemExit


def test_list_files_from_pattern():
    try:
        os.mkdir('test_dir')
        assert list_files_from_pattern('test_dir') == []
    finally:
        os.rmdir('test_dir')
    with pytest.raises(SystemExit) as cap_err:
        list_files_from_pattern('_aaa_A__A_A_')
    assert cap_err.type == SystemExit


def test_map_file():
    path = 'data/split_aa.fastq.data.json'
    assert map_file(sum_field, path=path, field='seqlen') == 247723
    assert map_file(sum_field, path='', field='seqlen') == 0


def test_sum_field():
    path = 'data/split_aa.fastq.data.json'
    assert map_file(sum_field, path=path, field='seqlen') == 247723
    content = StringIO("""{"seqlen": 20}\nrubbish\n{"seqlen": 10}""")
    params = {
            "content": content,
            "path": "test/path",
            "field": "seqlen"
            }
    assert sum_field(**params) == 30
