from unittest.mock import patch
from sumall.sumall import main


__doc__ = """
Sum all seqlen fields in all files for a given directory.

    Usage: sum_all directory_path

Given a directory path this program will sum all seqlen fields in all files
matching the following pattern *.data.json
"""


def test_main():
    with patch('sys.argv', ['sumall', 'data']):
        assert main() == 1324172
