main:
	tox
dev_install:
	pip3 install -e .
test:
	pytest --cov=sumall --cov-report term-missing tests/
