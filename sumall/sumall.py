"""
Sum all seqlen fields in all files for a given directory.

    Usage: sum_all directory_path

Given a directory path this program will sum all seqlen fields in all files
matching the following pattern *.data.json
"""
from .helpers import list_files_from_pattern
from .helpers import map_file
from .helpers import parse_arg
from .helpers import sum_field


def main():
    """
    Sum all seqlen fields in all files for a given directory.

    :param: directory: optional.
    :return: sum of all seqlen fields.
    """
    total = []

    for path in list_files_from_pattern(parse_arg(__doc__)):
        total.append(map_file(sum_field, path=path, field='seqlen'))
    total = sum(total)
    print(total)
    return total
